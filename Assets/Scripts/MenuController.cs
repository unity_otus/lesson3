using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    enum Screen
    {
        Main,
        Settings,
        Levels
    }

    public CanvasGroup mainScreen;
    public CanvasGroup settingsScreen;
    public CanvasGroup levelsScreen;
    
    void SetCurrentScreen(Screen screen)
    {
        Utility.SetCanvasGroupEnabled(mainScreen, screen == Screen.Main);
        Utility.SetCanvasGroupEnabled(settingsScreen, screen == Screen.Settings);
        Utility.SetCanvasGroupEnabled(levelsScreen, screen == Screen.Levels);
    }
    
    void Start()
    {
        SetCurrentScreen(Screen.Main);
    }

    public void StartNewGameFirst()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void StartNewGameSecond()
    {
        SceneManager.LoadScene("SampleSceneSecond");
    }
    
    public void OpenSettings()
    {
        SetCurrentScreen(Screen.Settings);
    }

    public void CloseSettings()
    {
        SetCurrentScreen(Screen.Main);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenLevels()
    {
        SetCurrentScreen(Screen.Levels);
    }

    public void GoBack()
    {
        SetCurrentScreen(Screen.Main);
    }
}
